# Serbian translation
# Copyright (C) 2016 VideoLAN
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# DakSrbija <vlabotami@gmail.com>, 2013
# Rancher <djordje.vasiljevich@gmail.com>, 2013
# Dzony95 <nikoladzonystojanovic@gmail.com>, 2014
# Rancher <djordje.vasiljevich@gmail.com>, 2013
# Предраг Љубеновић <predragljubenovic@gmail.com>, 2014
msgid ""
msgstr ""
"Project-Id-Version: VLC - Trans\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-12-03 06:41-0500\n"
"PO-Revision-Date: 2016-02-16 09:23+0000\n"
"Last-Translator: Christoph Miebach <christoph.miebach@web.de>\n"
"Language-Team: Serbian (http://www.transifex.com/yaron/vlc-trans/language/sr/)\n"
"Language: sr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: include/header.php:292
msgid "a project and a"
msgstr ""

#: include/header.php:292
msgid "non-profit organization"
msgstr "непрофитна организација"

#: include/header.php:301 include/footer.php:80
msgid "Partners"
msgstr "Партнери"

#: include/menus.php:32
msgid "Team &amp; Organization"
msgstr ""

#: include/menus.php:33
msgid "Consulting Services &amp; Partners"
msgstr ""

#: include/menus.php:34 include/footer.php:83
msgid "Events"
msgstr "Догађаји"

#: include/menus.php:35 include/footer.php:78 include/footer.php:112
msgid "Legal"
msgstr "Правна питања"

#: include/menus.php:36 include/footer.php:82
msgid "Press center"
msgstr "Центар за штампу"

#: include/menus.php:37 include/footer.php:79
msgid "Contact us"
msgstr "Контакт"

#: include/menus.php:43 include/os-specific.php:261
msgid "Download"
msgstr "Преузимање"

#: include/menus.php:44 include/footer.php:38
msgid "Features"
msgstr "Могућности"

#: include/menus.php:45 vlc/index.php:57 vlc/index.php:63
msgid "Customize"
msgstr ""

#: include/menus.php:47 include/footer.php:70
msgid "Get Goodies"
msgstr "Материјали"

#: include/menus.php:51
msgid "Projects"
msgstr "Пројекти"

#: include/menus.php:69 include/footer.php:44
msgid "All Projects"
msgstr "Сви пројекти"

#: include/menus.php:73 index.php:165
msgid "Contribute"
msgstr "Допринос"

#: include/menus.php:75
msgid "Getting started"
msgstr ""

#: include/menus.php:76 include/menus.php:94
msgid "Donate"
msgstr "Прилози"

#: include/menus.php:77
msgid "Report a bug"
msgstr "Пријава грешака"

#: include/menus.php:81
msgid "Support"
msgstr "Подршка"

#: include/footer.php:36
msgid "Skins"
msgstr "Маске"

#: include/footer.php:37
msgid "Extensions"
msgstr "Проширења"

#: include/footer.php:39 vlc/index.php:83
msgid "Screenshots"
msgstr "Снимци екрана"

#: include/footer.php:62
msgid "Community"
msgstr "Заједница"

#: include/footer.php:65
msgid "Forums"
msgstr "Форуми"

#: include/footer.php:66
msgid "Mailing-Lists"
msgstr "Дописне листе"

#: include/footer.php:67
msgid "FAQ"
msgstr "Најчешћа питања"

#: include/footer.php:68
msgid "Donate money"
msgstr "Приложите новац"

#: include/footer.php:69
msgid "Donate time"
msgstr "Посветите време"

#: include/footer.php:76
msgid "Project and Organization"
msgstr "Пројекат и организација"

#: include/footer.php:77
msgid "Team"
msgstr "Тим"

#: include/footer.php:81
msgid "Mirrors"
msgstr "Пресликани сервери"

#: include/footer.php:84
msgid "Security center"
msgstr "Центар за безбедност"

#: include/footer.php:85
msgid "Get Involved"
msgstr "Укључите се"

#: include/footer.php:86
msgid "News"
msgstr "Вести"

#: include/os-specific.php:91
msgid "Download VLC"
msgstr "Преузми VLC"

#: include/os-specific.php:97 include/os-specific.php:277 vlc/index.php:168
msgid "Other Systems"
msgstr "Други системи"

#: include/os-specific.php:242
msgid "downloads so far"
msgstr ""

#: include/os-specific.php:630
msgid "VLC is a free and open source cross-platform multimedia player and framework that plays most multimedia files as well as DVDs, Audio CDs, VCDs, and various streaming protocols."
msgstr "VLC је слободно развојно окружење и мултимедијални плејер отвореног кода који пушта већину медијских датотека, као и аудио и видео дискове и разне протоколе за емитовање токова аудио и видео садржаја."

#: include/os-specific.php:634
msgid "VLC is a free and open source cross-platform multimedia player and framework that plays most multimedia files, and various streaming protocols."
msgstr ""

#: index.php:4
msgid "VLC: Official site - Free multimedia solutions for all OS!"
msgstr "Званичан сајт VLC-а — слободна решења за све оперативне системе!"

#: index.php:26
msgid "Other projects from VideoLAN"
msgstr "Други пројекти од VideoLAN-а"

#: index.php:30
msgid "For Everyone"
msgstr "За све"

#: index.php:40
msgid "VLC is a powerful media player playing most of the media codecs and video formats out there."
msgstr "VLC је моћан медијски плејер који подржава већину медијских кодека и видео-формата."

#: index.php:53
msgid "VideoLAN Movie Creator is a non-linear editing software for video creation."
msgstr "VideoLAN Movie Creator је нелинеарни софтвер за стварање и уређивање видео-снимака."

#: index.php:62
msgid "For Professionals"
msgstr "За професионалце"

#: index.php:72
msgid "DVBlast is a simple and powerful MPEG-2/TS demux and streaming application."
msgstr "DVBlast је једноставан и моћан програм за MPEG-2/TS демултиплексирање и пуштање токова."

#: index.php:82
msgid "multicat is a set of tools designed to easily and efficiently manipulate multicast streams and TS."
msgstr "multicat је скуп алатки за лакше и ефикасније управљање токовима са усмеравањем на више адреса и TS-ом."

#: index.php:95
msgid "x264 is a free application for encoding video streams into the H.264/MPEG-4 AVC format."
msgstr "x264 је бесплатан програм за кодирање видео-токова у H.264/MPEG-4 AVC формат."

#: index.php:104
msgid "For Developers"
msgstr "За програмере"

#: index.php:137
msgid "View All Projects"
msgstr "Погледај све пројекте"

#: index.php:141
msgid "Help us out!"
msgstr "Помозите нам!"

#: index.php:145
msgid "donate"
msgstr "приложи"

#: index.php:153
msgid "VideoLAN is a non-profit organization."
msgstr "VideoLAN је непрофитна организација."

#: index.php:154
msgid " All our costs are met by donations we receive from our users. If you enjoy using a VideoLAN product, please donate to support us."
msgstr "Све наше трошкове покривају прилози које добијамо од корисника. Ако вам се свиђа производ VideoLAN-а, подржите нас новчаним прилогом."

#: index.php:157 index.php:177 index.php:195
msgid "Learn More"
msgstr "Сазнајте више"

#: index.php:173
msgid "VideoLAN is open-source software."
msgstr "VideoLAN је софтвер отвореног кода."

#: index.php:174
msgid "This means that if you have the skill and the desire to improve one of our products, your contributions are welcome"
msgstr "То значи да, ако имате жељу и знање да побољшате један од наших производа, можете то учинити"

#: index.php:184
msgid "Spread the Word"
msgstr "Ширите вест"

#: index.php:192
msgid "We feel that VideoLAN has the best video software available at the best price: free. If you agree please help spread the word about our software."
msgstr "Мислимо да VideoLAN поседује најбољи видео-софтвер на тржишту по најповољнијој цени — бесплатно. Ако се слажете с овом тврдњом, ширите вест о нашем програму."

#: index.php:212
msgid "News &amp; Updates"
msgstr "Вести и ажурирања"

#: index.php:215
msgid "More News"
msgstr ""

#: index.php:219
msgid "Development Blogs"
msgstr "Блогови о развоју"

#: index.php:248
msgid "Social media"
msgstr "Друштвене мреже"

#: vlc/index.php:3
msgid "Official page for VLC media player, the Open Source video framework!"
msgstr "Званична страница VLC Media Player-а, развојног окружења за видео отвореног кода!"

#: vlc/index.php:21
msgid "Get VLC for"
msgstr "Преузмите VLC за"

#: vlc/index.php:29 vlc/index.php:32
msgid "Simple, fast and powerful"
msgstr ""

#: vlc/index.php:35
msgid "Plays everything"
msgstr ""

#: vlc/index.php:35
msgid "Files, Discs, Webcams, Devices and Streams."
msgstr ""

#: vlc/index.php:38
msgid "Plays most codecs with no codec packs needed"
msgstr ""

#: vlc/index.php:41
msgid "Runs on all platforms"
msgstr ""

#: vlc/index.php:44
msgid "Completely Free"
msgstr ""

#: vlc/index.php:44
msgid "no spyware, no ads and no user tracking."
msgstr ""

#: vlc/index.php:47
msgid "learn more"
msgstr ""

#: vlc/index.php:66
msgid "Add"
msgstr ""

#: vlc/index.php:66
msgid "skins"
msgstr ""

#: vlc/index.php:69
msgid "Create skins with"
msgstr ""

#: vlc/index.php:69
msgid "VLC skin editor"
msgstr ""

#: vlc/index.php:72
msgid "Install"
msgstr ""

#: vlc/index.php:72
msgid "extensions"
msgstr ""

#: vlc/index.php:126
msgid "View all screenshots"
msgstr "Погледај све снимке екрана"

#: vlc/index.php:135
msgid "Official Downloads of VLC media player"
msgstr "Званична преузимања VLC Media Player-а"

#: vlc/index.php:146
msgid "Sources"
msgstr "Извори"

#: vlc/index.php:147
msgid "You can also directly get the"
msgstr "Можете и да директно преузмете"

#: vlc/index.php:148
msgid "source code"
msgstr "изворни кôд"

#~ msgid "A project and a"
#~ msgstr "Пројекат и"

#~ msgid "composed of volunteers, developing and promoting free, open-source multimedia solutions."
#~ msgstr "састављена од добровољаца који развијају и промовишу слободна мултимедијална решења отвореног кода."

#~ msgid "why?"
#~ msgstr "зашто?"

#~ msgid "Home"
#~ msgstr "Почетна"

#~ msgid "Support center"
#~ msgstr "Центар за подршку"

#~ msgid "Dev' Zone"
#~ msgstr "Зона за програмере"

#~ msgid "Simple, fast and powerful media player."
#~ msgstr "Једноставан, брз и моћан медијски плејер."

#~ msgid "Plays everything: Files, Discs, Webcams, Devices and Streams."
#~ msgstr "Репродукује све: датотеке, дискове, веб камере, уређаје и токове."

#~ msgid "Plays most codecs with no codec packs needed:"
#~ msgstr "Репродукује већину кодека без употребе пакета кодека:"

#~ msgid "Runs on all platforms:"
#~ msgstr "Ради на свим платформама:"

#~ msgid "Completely Free, no spyware, no ads and no user tracking."
#~ msgstr "Потпуно бесплатно, без шпијунских програма, реклама и праћења корисника."

#~ msgid "Can do media conversion and streaming."
#~ msgstr "Може да претвара медијске датотеке и пушта токове."

#~ msgid "Discover all features"
#~ msgstr "Откријте све могућности"

#~ msgid "DONATE"
#~ msgstr "ДАЈТЕ ПРИЛОГ"

#~ msgid "Other Systems and Versions"
#~ msgstr "Други системи и верзије"

#~ msgid "Other OS"
#~ msgstr "Други ОС"
