<?php
   $title = "VLC media player for Red Hat Linux";
   $new_design = true;
   $lang = "en";
   $menu = array( "vlc", "download" );

   $additional_js = array("/js/slimbox2.js", "/js/slick-init.js", "/js/slick.min.js");
   $additional_css = array("/js/css/slimbox2.css", "/style/slick.min.css", "/style/panels.css");
   require($_SERVER["DOCUMENT_ROOT"]."/include/header.php");
   include($_SERVER["DOCUMENT_ROOT"]."/include/os-specific.php");
   include($_SERVER["DOCUMENT_ROOT"]."/include/package.php");
?>

<div class="container">
	<?php
	$screenshots = getScreenshots("linux");
	$defaultDetail = getOS("linux");
	?>
    <section class="download-wrapper">
        <div class="row reorder-xs">
            <?php drawScreenshots($screenshots); ?>
            <div class="v-align col-sm-5">
                <div class="center-font-xs">
                    <?php image('largeVLC.png', 'Large Orange VLC media player Traffic Cone Logo', 'big-vlc-img img-responsive visible-xs-inline-block v-align'); ?>
                    <h1 class="v-align bigtitle">
                        VLC media player for <a href="http://www.redhat.com/rhel/">Red Hat Enterprise Linux</a>
                    </h1>
                </div>
                <div class="projectDescription hidden-sm hidden-xs">
                    <?php echo
                    _("VLC is a free and open source cross-platform multimedia player and framework that plays most multimedia files as well as DVDs, Audio CDs, VCDs, and various streaming protocols."); ?>
                </div>
                <div class="projectDescription visible-xs visible-sm center-font-xs">
                    <?php echo
                    _("VLC is a free and open source cross-platform multimedia player and framework that plays most multimedia files as well as DVDs, Audio CDs, VCDs, and various streaming protocols."); ?>
                </div>
            </div>
        </div>
    </section>

<div id="fullwidth">
<h2> 0.9.9 Packages for RHEL 5, x86 and x86_64 on RPMforge</h2>
<p>Use <a href="http://apt.sw.be/">RPMforge</a> to have 0.9.9 for those versions!</p>

<h2> 0.8.6 Packages for RHEL 4 and RHEL 5, x86 and x86_64 on RPMforge</h2>
<p>Use <a href="http://apt.sw.be/">RPMforge</a> to have 0.8.6 for those versions!</p>

<h2>Red Hat 9.0 and RHEL 3 packages for i386</h2>
<p>Those versions are not supported anymore. You can get some very old versions of VideoLan Client on RPMforge.</p>

<h2>Older versions</h2>
<?php browse_old( "vlc" ); ?>

</div>
</div>

<?php footer('$Id$'); ?>
